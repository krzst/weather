import {Component} from '@angular/core';
import {Observable} from "rxjs/index";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  weatherDetails: any;
  city: String

  private url = "http://api.openweathermap.org/data/2.5/forecast?q=Krakow,pl&units=metric&mode=json&appid=1474b92c8b547daa38d507963ff9a48c";

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.loadWeatherAndUpdate();
  }

  getWeatherForFiveDays(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  loadWeatherAndUpdate() {
    this.getWeatherForFiveDays().subscribe(data => {
      if (data && data.city && data.city.name) {
        this.city = data.city.name;
      }

      this.weatherDetails = {};
      var weatherItems = data.list;
      for (var idx in weatherItems) {
        var hourDetails = weatherItems[idx];
        var forecastDate = new Date(hourDetails['dt_txt'])
        var forecastDay = this.getOnlyDay(forecastDate);

        if (this.weatherDetails[forecastDay] == undefined || this.weatherDetails[forecastDay] == null) {
          this.weatherDetails[forecastDay] = new Array();
        }

        this.weatherDetails[forecastDay].push(hourDetails);
      }

      console.log(this.weatherDetails);
    });
  }

  getOnlyDay(date?: Date) {
    var month = date.getMonth();
    var day = date.getDate() ;
    var year = date.getFullYear();

    return month + "-" + day + "-" + year;
  }

  isTheSameDay(date1?: Date, date2?: Date) {
    return this.getOnlyDay(date1) === this.getOnlyDay(date2);
  }

  objectKeys(obj) {
    return Object.keys(obj);
  }
}
